﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float playerSpeed;
    private Vector2 axis;
    private bool imDead = false;
    void Update()
    {
        if(imDead){
            return;
        }

        //Movimiento private void OnPlayerConnected(NetworkPlayer player) {
            transform.Translate ( axis * playerSpeed * Time.deltaTime);
        }
        
        public void SetAxis(Vector2 currentAxis){
            axis = currentAxis;
        }
    }